;;; dir-var-test.el --- Directory variables -*- lexical-binding: t; -*-

;;; Copyright (C) 2023  BTuin
;;; Homepage: https://gitlab.com/btuin2/builder

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;; This package provides tests for dir-var-el.

;;; Code:

(require 'ert)
(require 'dir-var)


(ert-deftest dir-var-set-variable-test ()
  (should
   (equal
    (dir-var--set-node-variable
     (dir-var--set-node-variable
      (dir-var--make-node "name")
      'var "test")
     'var "value")
    '("name" nil (var "value")))))


(ert-deftest dir-var-insert-variable-test ()
  (should
   (equal
    (dir-var--insert-variable
     (dir-var--insert-variable
      (dir-var--make-node "test")
      (list "1" "2" "3" "4")
      'var1 "value1")
     (list "1" "2" "5")
     'var2 "value2")
   '("test"
     (("1"
       (("2"
	 (("5" nil
	   (var2 "value2"))
	  ("3"
	   (("4" nil
	     (var1 "value1")))))))))))))

(ert-deftest dir-var-remove-variable-test ()
  (should
   (equal
    (dir-var--remove-variable
     (dir-var--insert-variable
      (dir-var--insert-variable
       (dir-var--make-node "test")
       (list "1" "2" "3" "4")
       'var1 "value1")
      (list "1" "2" "5")
      'var2 "value2")
     (list "1" "2" "3" "4")
     'var1)
    '("test"
      (("1"
        (("2"
	  (("5" nil
	    (var2 "value2")))))))))))


(ert-deftest dir-var-get-variable-test ()
  (let ((tree (dir-var--make-node "test")))
    (setq tree (dir-var--insert-variable
                tree
                (list "1" "2" "3" "4")
                'var1 "value1"))
    (setq tree (dir-var--insert-variable
                tree
                (list "1" "2" "5")
                'var2 "value2"))
    (setq tree (dir-var--insert-variable
                tree
                (list "1" "2" "3")
                'var3 "value3"))
    (should
     (equal
      (dir-var--variable-get-value
       tree
       (list "1" "2" "3" "4")
       'var1)
      "value1"))
    (should
     (equal
      (dir-var--variable-get-value
       tree
       (list "1" "2" "5")
       'var2)
      "value2"))
    (should
     (equal
      (dir-var--variable-get-value
       tree
       (list "1" "2" "3" "4")
       'var3)
      "value3"))
    (should
     (equal
      (dir-var--variable-get-value
       tree
       (list "1" "2" "5")
       'var3)
      nil))))

(ert-deftest dir-var-insert-test ()
  (let* ((directory "/example/path/to/dir")
         (value "some value")
         ;; Shadow the global variable dir-var--tree to avoid polluting it
         (dir-var--tree))
    (dir-var-insert 'variable value directory)
    (should (equal (dir-var-get 'variable directory) value))
    (should (equal (dir-var-get 'variable
                                (file-name-directory directory))
                   nil))))

(ert-deftest dir-var-variable-exists-test ()
  (let* ((directory1 "/example/path/to/dir")
         (parent-directory1 "/example/path/")
         (directory2 "/other/path/to/dir")
         (value1 "some value")
         (value2 "other value")
         ;; Shadow the global variable dir-var--tree to avoid polluting it
         (dir-var--tree))
    (dir-var-insert 'variable1 value1 parent-directory1)
    (dir-var-insert 'variable2 value2 directory2)
    (should (dir-var-variable-exists-p 'variable1 parent-directory1))
    (should (dir-var-variable-exists-p 'variable1 directory1))
    (should (dir-var-variable-exists-p 'variable2 directory2))
    (should-not (dir-var-variable-exists-p 'variable1 directory2))))

(ert-deftest dir-var-shadowing-test ()
  (let* ((directory "/example/path/to/dir")
         (parent-directory "/example/path/")
         (value1 "some value")
         (value2 "other value")
         ;; Shadow the global variable dir-var--tree to avoid polluting it
         (dir-var--tree))
    (dir-var-insert 'variable value1 parent-directory)
    (dir-var-insert 'variable value2 directory)
    (should (equal value1 (dir-var-get 'variable parent-directory)))
    (should (equal value2 (dir-var-get 'variable directory)))))


(provide 'dir-var-test)
;;; dir-var-test.el ends here
